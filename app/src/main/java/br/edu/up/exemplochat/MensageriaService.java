package br.edu.up.exemplochat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MensageriaService extends FirebaseMessagingService {


  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {


    RemoteMessage.Notification msg = remoteMessage.getNotification();

    if (msg != null){

      Intent intent = new Intent(this, MainActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
          intent, PendingIntent.FLAG_ONE_SHOT);

      NotificationCompat.Builder builder = new NotificationCompat
          .Builder(this)
          .setSmallIcon(R.mipmap.ic_launcher)
          .setContentTitle(msg.getTitle())
          .setContentText(msg.getBody())
          .setContentIntent(pendingIntent)
          .setAutoCancel(true);

      NotificationManager manager = (NotificationManager)
          getSystemService(NOTIFICATION_SERVICE);
      manager.notify(0, builder.build());

    }
  }
}

