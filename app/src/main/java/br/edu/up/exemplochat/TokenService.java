package br.edu.up.exemplochat;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class TokenService extends FirebaseInstanceIdService {

  @Override
  public void onTokenRefresh() {

    String token = FirebaseInstanceId.getInstance().getToken();
    Log.d("TOKEN", token);
  }
}
