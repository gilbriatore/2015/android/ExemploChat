package br.edu.up.exemplochat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


  DatabaseReference root;
  TextView txtChat;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    root = FirebaseDatabase.getInstance().getReference();

    txtChat = (TextView) findViewById(R.id.txtChat);
    final EditText txtMensagem = (EditText) findViewById(R.id.txtMensagem);
    Button btnEnviar = (Button) findViewById(R.id.btnEnviar);

    btnEnviar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        DatabaseReference child = root.child("mensagens");

        Map<String, Object> map = new HashMap<>();
        map.put("usuario","app");
        map.put("mensagem", txtMensagem.getText().toString());

        child.updateChildren(map);

        txtMensagem.setText("");
      }
    });


    root.addChildEventListener(new ChildEventListener() {
      @Override
      public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        atualizarChat(dataSnapshot);
      }

      @Override
      public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        atualizarChat(dataSnapshot);
      }

      @Override
      public void onChildRemoved(DataSnapshot dataSnapshot) {
        //Não implmentado ainda..
      }

      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        //Não implmentado ainda..
      }

      @Override
      public void onCancelled(DatabaseError databaseError) {
        //Não implmentado ainda..
      }
    });




  }

  private void atualizarChat(DataSnapshot dataSnapshot) {

    Iterator iterator = dataSnapshot.getChildren().iterator();

    while(iterator.hasNext()){

      DataSnapshot ds1 = (DataSnapshot) iterator.next();
      String mensagem = ds1.getValue(String.class);

      DataSnapshot ds2 = (DataSnapshot) iterator.next();
      String usuario = ds2.getValue(String.class);

      txtChat.append(usuario + ": " + mensagem  + "\n");

    }

  }
}