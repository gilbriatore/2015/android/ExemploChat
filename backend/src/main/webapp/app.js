$(document).ready(function () {

    $('#button-send').click(function () {
        enviarMensagem();
    });

    $(document).keypress(function (e) {
        if (e.which == 13) {
            enviarMensagem();
        }
    });

    var db = firebase.database();
    var root = db.ref('mensagens');
    root.on('value', function(snapshot) {
        var dados = snapshot.val();
        if (dados != null){
          var il = "<li class='mdl-list__item'>" + dados.usuario + ": " + dados.mensagem + "</li>";
          $('#mensagens').append(il);
        }
    });

    function enviarMensagem() {
        
        var mensagem = $('#input-message').val();    
        if (mensagem.trim() == '') {
            return
        };
        var data = {
            usuario: "Web", 
            mensagem: mensagem
        }; 
        root.update(data);
        $('#input-message').val('');
        $('#input-message').focus();
    }
});