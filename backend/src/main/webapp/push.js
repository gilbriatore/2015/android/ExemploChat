$("#button").click(function(){

  var mensagem = $("#mensagem").val();
  $("#mensagem").val("");

  var json = {
    //Dispositivo de destino (ver FirebaseInstanceIdService).
    "to": "fq6mS62kCFw:APA91bHLt86eTmu-hVOtJQt2P-689T5XsYioqGcLfKDDilLnSedT7Y_gyEkBp6HrsnOswFf7oEvv12cgIbAG6--cyW72Z-v6f84NnI4h_B7Dy5UTKepP0Ib1iJ8DWMoOQv3l8-wEokmD",
    "notification": {
       "title": "Push enviado com JQuery",
       "body": mensagem
    },
    "data": {
       "title": "Título de dados",
       "body": "corpo de dados...."
    }
  };

  $.ajax({
    //Url do Firebase para envio de mensagens push.
    url: 'https://fcm.googleapis.com/fcm/send',
    type: "POST",
    processData : false,
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Content-Type', 'application/json');
       //Server key do Cloud Messaging (ver Firebase Console).
      xhr.setRequestHeader('Authorization', 'key=AIzaSyCvU37y3L1Cgab2zdKL2P8dnUMt8RfNQfY');
    },
    data: JSON.stringify(json),
    success: function () {
     console.log("Mensagem enviada com sucesso!");
    },
    error: function(error) {
     console.log(error);
    }
  });
});